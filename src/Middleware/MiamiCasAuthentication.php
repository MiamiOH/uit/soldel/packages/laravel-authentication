<?php

namespace MiamiOH\Authentication\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use MiamiOH\Authentication\Exceptions\CasAuthenticationOnJsonRequestException;
use Subfission\Cas\CasManager;

class MiamiCasAuthentication
{

    /** @var Guard */
    protected $auth;
    /** @var CasManager */
    protected $cas;

    /**
     * MiamiCasAuthentication constructor.
     * @param Guard $auth
     * @param CasManager|null $casManager
     */
    public function __construct(Guard $auth, CasManager $casManager = null)
    {
        if ($casManager === null) {
            $casManager = resolve('cas');
        }

        $this->auth = $auth;
        $this->cas = $casManager;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        try {
            $this->checkAuthentication($request);
        } catch (CasAuthenticationOnJsonRequestException $e) {
            return response('Unauthorized.', 401);
        }

        return $next($request);
    }

    /**
     * @param Request $request
     * @throws CasAuthenticationOnJsonRequestException
     */
    private function checkAuthentication(Request $request): void
    {
        if ($this->casDisabled()) {
            return;
        }

        if ($this->casAuthenticated()) {
            return;
        }

        $this->authenticate($request);

    }

    /**
     * @return bool
     */
    private function casDisabled(): bool
    {
        return !config('miamioh-authn.cas_enabled', true);
    }

    /**
     * @return bool
     */
    private function casAuthenticated(): bool
    {
        if (!$this->cas->checkAuthentication()) {
            return false;
        }

        $this->loginCasUser();

        return true;
    }

    /**
     * @param Request $request
     * @throws CasAuthenticationOnJsonRequestException
     */
    private function authenticate(Request $request): void
    {
        if ($request->ajax() || $request->wantsJson()) {
            throw new CasAuthenticationOnJsonRequestException();
        }

        $this->cas->authenticate();
    }

    private function loginCasUser(): void
    {
        if (Auth::check()) {
            return;
        }

        Auth::loginUsingId($this->cas->user());
    }
}