<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 6/12/18
 * Time: 7:16 AM
 */

namespace MiamiOH\Authentication;


use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\GenericUser;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

/**
 * Class MiamiUser
 *
 * @property-read int $id
 * @property-read string $username
 * @property-read string $displayName
 */
class MiamiUser extends GenericUser implements AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * @return string
     */
    public function getKeyName(): string
    {
        return 'username';
    }

    /**
     * @return |null
     */
    public function getMorphClass()
    {
        return null;
    }
}
