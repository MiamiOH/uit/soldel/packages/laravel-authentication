<?php

namespace MiamiOH\Authentication\Testing;


use MiamiOH\Authentication\MiamiAuthenticationServiceProvider;
use PHPUnit\Framework\MockObject\MockObject;
use Subfission\Cas\CasManager;

/**
 * Trait AuthenticateCasUser
 * @package MiamiOH\Authentication\Testing
 *
 * @codeCoverageIgnore
 */
trait AuthenticateCasUser
{
    /** @var CasManager|MockObject */
    protected $casManager;

    protected $casUserIdentifier = '';
    protected $casUserAttributes = [];

    protected function authenticateWithCas(): void
    {
        $this->createCasManagerMock();
    }

    /**
     * @param string $identifier
     * @param array $attributes
     */
    protected function useAuthenticatedUser(string $identifier, array $attributes = []): void
    {
        $this->createCasManagerMock();

        $this->setCasUserIdentifier($identifier);

        $this->setCasUserAttributes($attributes);

        $this->loginCasUser();
    }

    private function createCasManagerMock(): void
    {
        if (null !== $this->casManager) {
            return;
        }

        MiamiAuthenticationServiceProvider::enableCasManagerInConsole();
        $this->casManager = $this->createMock(CasManager::class);
        $this->app->instance('cas', $this->casManager);
    }

    /**
     * @param string $identifier
     */
    private function setCasUserIdentifier(string $identifier): void
    {
        $this->casUserIdentifier = $identifier;
    }

    /**
     * @param array $attributes
     */
    private function setCasUserAttributes(array $attributes): void
    {
        $defaultAttributes = [
            'id' => null,
            'uid' => $this->casUserIdentifier,
            'uidNumber' => null,
        ];

        $this->casUserAttributes = array_merge($defaultAttributes, $attributes);

        $this->casManager->method('getAttributes')->willReturn($this->casUserAttributes);
    }

    private function loginCasUser(): void
    {
        $this->casManager->method('checkAuthentication')->willReturn(true);
        $this->casManager->method('user')->willReturn($this->casUserIdentifier);
    }
}
