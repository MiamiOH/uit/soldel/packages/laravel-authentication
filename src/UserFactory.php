<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 2019-03-10
 * Time: 10:41
 */

namespace MiamiOH\Authentication;


use MiamiOH\Authentication\Exceptions\IdentifierMatchException;
use Subfission\Cas\CasManager;

class UserFactory
{
    /**
     * @var CasManager
     */
    private $casManager;

    /**
     * UserFactory constructor.
     * @param CasManager $casManager
     */
    public function __construct(CasManager $casManager)
    {
        $this->casManager = $casManager;
    }

    /**
     * @param string $identifier
     * @return MiamiUser
     * @throws IdentifierMatchException
     */
    public function createCasUser(string $identifier): MiamiUser
    {
        $attributes = $this->casManager->getAttributes();

        $this->validateIdentifier($identifier, $attributes);

        $attributes['id'] = $attributes['uidNumber'];
        $attributes['username'] = $attributes['uid'];

        //check for impersonated logins

        return $this->makeUser($attributes);
    }

    /**
     * @param string $identifier
     * @param array $attributes
     * @throws IdentifierMatchException
     */
    private function validateIdentifier(string $identifier, array $attributes): void
    {
        $uid = $attributes['uid'] ?? '';

        if ($identifier !== $uid) {
            throw new IdentifierMatchException(sprintf('Incoming identifier "%s" does not match CAS user "%s"', $identifier, $uid));
        }
    }

    /**
     * @param array $attributes
     * @return MiamiUser
     */
    private function makeUser(array $attributes): MiamiUser
    {
        return new MiamiUser($this->makeUserAttributes($attributes));
    }

    /**
     * @param array $attributes
     * @return array
     */
    private function makeUserAttributes(array $attributes): array
    {
        $defaults = [
            'id' => 0,
            'password' => '',
            'remember_token' => '',
            'username' => '',
            'displayName' => '',
        ];

        return array_merge($defaults, $attributes);
    }
}