<?php

namespace MiamiOH\Authentication;

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

/**
 * Class MiamiAuthenticationServiceProvider
 * @package MiamiOH\Authentication
 *
 * @codeCoverageIgnore
 */
class MiamiAuthenticationServiceProvider extends ServiceProvider
{
    private $configPath = __DIR__ . '/../config/miamioh-authn.php';

    private static $disableCasManagerInConsole = true;

    public static function enableCasManagerInConsole(): void
    {
        self::$disableCasManagerInConsole = false;
    }

    public static function disableCasManagerInConsole(): void
    {
        self::$disableCasManagerInConsole = true;
    }

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            $this->configPath => config_path('miamioh-authn.php'),
        ]);

        Auth::provider('MiamiOH', function(Application $app, array $config) {
            if ($this->disableCasManager()) {
                return $this->app->make(config('miamioh-authn.fallback-user-provider', MiamiUserProvider::class));
            }

            return new MiamiUserProvider(new UserFactory($app->make('cas')));
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(\Subfission\Cas\CasServiceProvider::class);

        $this->mergeConfigFrom(
            $this->configPath, 'miamioh-authn'
        );
    }

    private function disableCasManager(): bool
    {
        if ($this->app->runningInConsole()) {
            return self::$disableCasManagerInConsole;
        }

        /*
         * If a specific middleware is required, and the current route does not include it, disable CAS
         */
        $requiredMiddleware = config('miamioh-authn.required-middleware', []);
        $routeMiddleware = optional(Route::current())->gatherMiddleware() ?? [];

        return !empty($requiredMiddleware) && empty(array_intersect($requiredMiddleware, $routeMiddleware));
    }
}