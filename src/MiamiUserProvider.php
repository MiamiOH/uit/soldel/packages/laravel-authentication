<?php

namespace MiamiOH\Authentication;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Support\Facades\App;


class MiamiUserProvider implements UserProvider
{
    /**
     * @var UserFactory|null
     */
    private $userFactory;

    public function __construct(?UserFactory $userFactory = null)
    {
        $this->userFactory = $userFactory;
    }

    /**
     * @param $identifier
     * @return Authenticatable|MiamiUser|null
     * @throws Exceptions\IdentifierMatchException
     */
    public function retrieveById($identifier)
    {
        if (null === $this->userFactory) {
            return null;
        }

        return $this->userFactory->createCasUser($identifier);
    }

    /**
     * @param mixed $identifier
     * @param string $token
     * @return \Exception
     *
     * @codeCoverageIgnore
     */
    public function retrieveByToken($identifier, $token)
    {
        return new \Exception('not implemented');
    }

    /**
     * @param Authenticatable $user
     * @param string $token
     * @return \Exception
     *
     * @codeCoverageIgnore
     */
    public function updateRememberToken(Authenticatable $user, $token)
    {
        return new \Exception('not implemented');
    }

    /**
     * @param array $credentials
     * @return \Exception
     *
     * @codeCoverageIgnore
     */
    public function retrieveByCredentials(array $credentials)
    {
        return new \Exception('not implemented');
    }

    /**
     * @param Authenticatable $user
     * @param array $credentials
     * @return \Exception
     *
     * @codeCoverageIgnore
     */
    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        return new \Exception('not implemented');
    }

    public function rehashPasswordIfRequired(Authenticatable $user, array $credentials, bool $force=false){
        return new \Exception('not implemented');

    }
}
