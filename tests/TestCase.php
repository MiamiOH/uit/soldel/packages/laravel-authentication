<?php
/**
 * Created by PhpStorm.
 * User: tepeds
 * Date: 2019-03-09
 * Time: 20:45
 */

namespace MiamiOH\Authentication\Test;


use MiamiOH\Authentication\MiamiAuthenticationServiceProvider;
use Orchestra\Testbench\TestCase as OrchestraTestCase;

class TestCase extends OrchestraTestCase
{
    protected function getPackageProviders($app)
    {
        return [MiamiAuthenticationServiceProvider::class];
    }

    protected function getPackageAliases($app)
    {
        return [
        ];
    }
}
