<?php

namespace MiamiOH\Authentication\Test\Unit;

use MiamiOH\Authentication\MiamiUser;
use PHPUnit\Framework\TestCase;

class MiamiUserTest extends TestCase
{
    public function testReturnsCorrectKeyName(): void
    {
        $miamiUser = new MiamiUser([]);

        $this->assertEquals('username', $miamiUser->getKeyName());
    }

    public function testReturnsCorrectMorphClass():void
    {
        $miamiUser = new MiamiUser([]);

        $this->assertNull($miamiUser->getMorphClass());
    }
}
