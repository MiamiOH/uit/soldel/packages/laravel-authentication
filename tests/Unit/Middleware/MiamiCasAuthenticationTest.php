<?php

namespace MiamiOH\Authentication\Test\Unit\Middleware;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use MiamiOH\Authentication\Middleware\MiamiCasAuthentication;
use MiamiOH\Authentication\Test\TestCase;
use PHPUnit\Framework\MockObject\MockObject;
use Subfission\Cas\CasManager;

class MiamiCasAuthenticationTest extends TestCase
{
    /** @var Guard|MockObject */
    private $guard;
    /** @var CasManager|MockObject */
    private $cas;

    /** @var MiamiCasAuthentication */
    private $middleware;

    public function setUp(): void
    {
        parent::setUp();

        $this->guard = $this->createMock(Guard::class);
        $this->cas = $this->createMock(CasManager::class);

        $this->middleware = new MiamiCasAuthentication($this->guard, $this->cas);
    }

    public function testDoesNotUseCasWhenDisabled(): void
    {
        Config::set('miamioh-authn.cas_enabled', false);

        $mainRequest = $this->makeRequest();
        $closure = function (Request $nextRequest) use ($mainRequest) {
            $this->assertEquals($mainRequest, $nextRequest);
        };

        $this->cas->expects($this->never())->method('checkAuthentication');

        $this->middleware->handle($mainRequest, $closure);
    }

    public function testReturnsUnauthorizedForJsonRequest(): void
    {
        $mainRequest = $this->makeJsonRequest();
        $closure = function (Request $nextRequest) use ($mainRequest) {
            $this->fail('Unexpected call to next middleware');
        };

        $this->cas->expects($this->once())
            ->method('checkAuthentication')
            ->willReturn(false);

        $this->cas->expects($this->never())->method('authenticate');

        /** @var Response $response */
        $response = $this->middleware->handle($mainRequest, $closure);

        $this->assertEquals(401, $response->status());
    }

    public function testCallsCasToAuthenticateUser(): void
    {
        $mainRequest = $this->makeRequest();
        $closure = function (Request $nextRequest) use ($mainRequest) {
            $this->assertEquals($mainRequest, $nextRequest);
        };

        $this->cas->expects($this->once())
            ->method('checkAuthentication')
            ->willReturn(false);

        $this->cas->expects($this->once())->method('authenticate');

        $this->middleware->handle($mainRequest, $closure);
    }

    public function testLogsUserIntoAppWhenAuthenticatedInCas(): void
    {
        $mainRequest = $this->makeRequest();
        $closure = function (Request $nextRequest) use ($mainRequest) {
            $this->assertEquals($mainRequest, $nextRequest);
        };

        $this->cas->expects($this->once())
            ->method('checkAuthentication')
            ->willReturn(true);

        $this->cas->expects($this->never())->method('authenticate');

        Auth::shouldReceive('check')->once()->andReturn(false);
        Auth::shouldReceive('loginUsingId')->once();

        $this->middleware->handle($mainRequest, $closure);
    }

    public function testDoesNotLogUserIntoAppIfAlreadyLoggedIn(): void
    {
        $mainRequest = $this->makeRequest();
        $closure = function (Request $nextRequest) use ($mainRequest) {
            $this->assertEquals($mainRequest, $nextRequest);
        };

        $this->cas->expects($this->once())
            ->method('checkAuthentication')
            ->willReturn(true);

        $this->cas->expects($this->never())->method('authenticate');

        Auth::shouldReceive('check')->once()->andReturn(true);
        Auth::shouldReceive('loginUsingId')->never();

        $this->middleware->handle($mainRequest, $closure);
    }

    private function makeRequest(): Request
    {
        return Request::create('https://example.com', 'GET');
    }

    private function makeJsonRequest(): Request
    {
        return Request::create('https://example.com', 'GET', [], [], [],['HTTP_ACCEPT' => 'application/json']);
    }
}
