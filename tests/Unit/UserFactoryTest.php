<?php

namespace MiamiOH\Authentication\Test\Unit;

use MiamiOH\Authentication\Exceptions\IdentifierMatchException;
use MiamiOH\Authentication\MiamiUser;
use MiamiOH\Authentication\Test\TestCase;
use MiamiOH\Authentication\UserFactory;
use PHPUnit\Framework\MockObject\MockObject;
use Subfission\Cas\CasManager;

class UserFactoryTest extends TestCase
{
    /** @var CasManager|MockObject */
    private $cas;

    /** @var UserFactory */
    private $factory;

    public function setUp(): void
    {
        parent::setUp();

        $this->cas = $this->createMock(CasManager::class);

        $this->factory = new UserFactory($this->cas);
    }

    public function testThrowsExceptionWhenIdentifyAndCasIdDoNotMatch(): void
    {
        $this->cas->expects($this->once())->method('getAttributes')
            ->willReturn([
                'uid' => 'bubba',
            ]);

        $this->expectException(IdentifierMatchException::class);

        $this->factory->createCasUser('bob');
    }

    public function testCanCreateUserFromCasAttributes(): void
    {
        $this->cas->expects($this->never())->method('getAttribute');
        $this->cas->expects($this->once())->method('getAttributes')
            ->willReturn([
                'uid' => 'bob',
                'uidNumber' => 123,
            ]);

        $user = $this->factory->createCasUser('bob');

        $this->assertInstanceOf(MiamiUser::class, $user);
        $this->assertEquals(123, $user->id);
        $this->assertEquals('bob', $user->username);
    }

    public function testSetsDisplayNameFromCas(): void
    {
        $this->cas->method('getAttributes')
            ->willReturn([
                'uid' => 'bob',
                'uidNumber' => 123,
                'displayName' => 'Bob Smith',
            ]);

        $user = $this->factory->createCasUser('bob');

        $this->assertEquals('Bob Smith', $user->displayName);
    }
}
