<?php

namespace MiamiOH\Authentication\Test\Unit;

use Illuminate\Support\Facades\App;
use MiamiOH\Authentication\MiamiUser;
use MiamiOH\Authentication\MiamiUserProvider;
use MiamiOH\Authentication\Test\TestCase;
use MiamiOH\Authentication\UserFactory;
use PHPUnit\Framework\MockObject\MockObject;

class MiamiUserProviderTest extends TestCase
{
    /** @var UserFactory|MockObject */
    private $userFactory;

    /** @var MiamiUserProvider */
    private $provider;

    public function setUp(): void
    {
        parent::setUp();

        $this->userFactory = $this->createMock(UserFactory::class);

        $this->provider = new MiamiUserProvider($this->userFactory);
    }

    public function testCreatesMiamiUser(): void
    {
        $testId = 'doej';

        $this->userFactory->expects($this->once())
            ->method('createCasUser')
            ->with($this->equalTo($testId));

        $user = $this->provider->retrieveById($testId);

        $this->assertInstanceOf(MiamiUser::class, $user);
    }

    public function testReturnsNullWithoutUserProvider(): void
    {
        $provider = new MiamiUserProvider();

        $this->assertNull($provider->retrieveById('doej'));
    }
}
