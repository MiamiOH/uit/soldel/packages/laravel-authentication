<?php

$requiredMiddleware = env('CAS_REQUIRED_MIDDLEWARE');

return [
    'cas_enabled' => env('CAS_ENABLED', true),
    'required-middleware' => empty($requiredMiddleware) ? null : explode(',', $requiredMiddleware),
    'fallback-user-provider' => env('CAS_FALLBACK_USER_PROVIDER', \MiamiOH\Authentication\MiamiUserProvider::class),
];
