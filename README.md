# Laravel Authentication

Provides authentication services for Miami users. The primary form of authentication is currently CAS.

## Installation

The package can be installed from Miami's satis server using composer and supports normal Laravel configuration conventions.

### Add Package

Require as a dependency:

```
composer require miamioh/laravel-authentication
```

### Configure CAS

The following env variables are related to the CAS configuration.

```
CAS_HOSTNAME=auth.miamioh.edu
CAS_REAL_HOSTS=auth.miamioh.edu
CAS_LOGOUT_URL=https://auth.miamioh.edu/cas/logout
CAS_VERSION=3.0
CAS_ENABLE_SAML=false
```

If desired, you can publish and modify the CAS configuration from the Subfission package:

```
php artisan vendor:publish
```

Publish either the `Subfission\Cas\CasServiceProvider` or the `config` tag. 

### Use the Miami User Provider

Update the user provider in `config/auth.php` to use the Miami provider.

```php
'providers' => [
    'users' => [
        'driver' => 'MiamiOH',
    ],
```

### Add Route Middleware

Update your `app/Http/Kernel.php` to include the CAS route middleware:

```php
protected $routeMiddleware = [
    ...
    'cas.miamioh' => \MiamiOH\Authentication\Middleware\MiamiCasAuthentication::class,
];
```

Note that in some cases, you may also need to explicitly manage the middleware order in `$middlewarePriority`. For example, when using the Laravel Menus middleware with user based permissions, the CAS middleware must execute first in order for the user identity to be established.

### Apply Middleware to Routes

Apply the middleware to your routes:

```php
Route::group(['middleware' => 'cas.miamioh'], function() {
    Route::get('/', function () {
        return view('welcome');
    });
});
```

### Miami User Provider and Non-CAS Routes

The Miami User Provider is used for all application processes by default. This includes API routes or unauthenticated routes. If the any step of the process requests the current user object (for example `$request->user()`), the CAS middleware may be triggered and cause unexpected issues. You can mitigate this situation by specifying a route middleware which must be present in order for the Miami User Provider to expect an authenticated CAS user. The middleware should match the label used to register your route middleware in `app/Http/Kernel.php`. For example, adding this to your `.env` will require a route to have the `cas.miamioh` middleware applied:

```shell
CAS_REQUIRED_MIDDLEWARE=cas.miamioh
```

This works if you have used the `Route::group()` or other means to add the middleware to your routes. If you have instead added the middleware to the Laravel `web` group, you should specify `web` as your required middleware.

### Miami User Provider and Console Commands

The Miami User Provider is explicitly configured to disable the CAS user resolution when running in a console command.

## The MiamiUser Object

The `MiamiUserProvider` creates a `MiamiUser` object for the logged in user. This class extends `GenericUser` and the Illuminate `Authorizable` contract. This allows the class to be used as a normal Laravel user object. For example, in a gate:

```php
Gate::define('allow-access', function (MiamiUser $user) {
    return $this->checkAuthorization($user->username);
});
```
The MiamiUser object is populated with data released by CAS to the application. There are two special cases. The 'uid' value (uniqueId) is set as the `username` of the MiamiUser object.

The `uidNumber` is mapped to `id` of the MiamiUser object. Laravel User objects typically have a numeric id and using the uidNumber minimizes potential problems with plugins. It should be noted however, that some account types, such as family members, do not have valid uidNumbers.

Attributes are currently mapped selectively and annotated on the class to help IDEs. Additional attributes should be added to the class as needed. The class should be enhanced with any special processing requirements as well. The class should not expose the attributes directly in order to prevent impact to applications from later changes.
 
## Feature Testing

The CAS service makes certain assumptions regarding the execution environment that are not compatible with typical testing scenarios. The package provides a trait which can be used during testing to provide mock CAS authentication services.

### Set Up

The trait can be used in any test classes, though a base test class is most convenient.

```php
use MiamiOH\Authentication\Testing\AuthenticateCasUser;

class FeatureTestCase extends TestCase
{
    use AuthenticateCasUser;
}
```

If the CAS service is not correctly mocked, you'll see errors such as `internal script error`. It may be helpful to put CAS into verbose mode to see more details errors by adding to your `.env.testing` file:

```
CAS_VERBOSE_ERRORS=true
```

### Authenticating a User

The minimum configuration simply requires a username:

```php
public function testListsProjects(): void
{
    $this->useAuthenticatedUser('doej');
    ...
}
```
If your application makes use of specific attributes from CAS, you may pass them as an array:

```php
public function testListsProjects(): void
{
    $this->useAuthenticatedUser('doej', [
        'uidNumber' => 123,
        'displayName' => 'John Doe',
    ]);
    ...
}
```
